package io.swagger.domain;


import javax.persistence.*;



@Entity
public class PlaylistSongEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;

	@Column(name="sid")
	public Integer sId;

	@ManyToOne
    @JoinColumn(name="pid", nullable=false)
    private PlaylistEntity playlist;



	public PlaylistEntity getPlaylist() {
		return playlist;
	}


	public void setPlaylist(PlaylistEntity playlist) {
		this.playlist = playlist;
	}

	
}