package io.swagger.domain;


import java.util.List;
import javax.persistence.*;



@Entity
public class PlaylistEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="username")
    public String username;
    
    @OneToMany(mappedBy="playlist", cascade=CascadeType.ALL)
    public List<PlaylistSongEntity> songs;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}