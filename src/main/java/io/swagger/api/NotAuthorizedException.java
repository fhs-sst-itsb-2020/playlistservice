package io.swagger.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-11-26T17:46:13.799Z")

public class NotAuthorizedException extends ApiException {
    private int code;
    public NotAuthorizedException(int code, String msg) {
        super(code, msg);
        this.code = code;
    }
}
