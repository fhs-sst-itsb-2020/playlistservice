package io.swagger.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.model.*;
import io.swagger.model.Playlist;
import io.swagger.model.Playlists;
import io.swagger.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.*;

@Controller
public class PlaylistsApiControllerImpl extends PlaylistsApiController {


    @Autowired
    public PlaylistsApiControllerImpl(ObjectMapper objectMapper, HttpServletRequest request) {
        super(objectMapper, request);
    }


    @Autowired
    private PlaylistService playlistService;

    @Autowired
    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }


    private Authentication getAuthenticatedUser() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private boolean isAdminUser() {

        for(GrantedAuthority gr : getAuthenticatedUser().getAuthorities()) {
            if (gr.getAuthority().equalsIgnoreCase("ROLE_ADMIN"))
                return true;
        }

        return false;

    }


    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Playlist> getPlaylistById(@Size(min=1)
                                                    @ApiParam(value = "id",required=true)
                                                    @PathVariable("pId") Integer pId) {

        try {
            return new ResponseEntity<>(playlistService.getPlaylistById(pId, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } 

    }
    
    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Playlists> playlistsGet() {

        try {
            if (isAdminUser())
                return new ResponseEntity<>(playlistService.getAllPlaylists(), HttpStatus.OK);
            else
                return new ResponseEntity<>(playlistService.getAllPlaylistsOfUser(getAuthenticatedUser().getName()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    
    
    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Playlist> playlistsPost(@NotNull @ApiParam(value = "Name of Playlist", required = true)
    											  @Valid @RequestParam(value = "name", required = true) String name) {
    
    	try {
            return new ResponseEntity<>(playlistService.addPlaylist(name, getAuthenticatedUser().getName()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }  	
    	
    }
    
    
    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Void> playlistsPIdSongsSIdPost(@Size(min=1)
                                                         @ApiParam(value = "id",required=true)
    											         @PathVariable("pId") Integer pId,
                                                         @Size(min=1)
    											         @ApiParam(value = "id",required=true)
                                                         @PathVariable("sId") Integer sId) {


        try {
            return new ResponseEntity<>(playlistService.addSongToPlaylist(pId, sId, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }  	
    	
    }


    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Void> playlistsPIdDelete(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId) {


        try {
            return new ResponseEntity<>(playlistService.deletePlaylist(pId, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Void> playlistsPIdPut(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId,
                                                @NotNull @ApiParam(value = "Name of Playlist", required = true)
                                                @Valid @RequestParam(value = "name", required = true) String name) {

        try {
            return new ResponseEntity<>(playlistService.changePlaylist(pId, name, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Void> playlistsPIdSongsSIdDelete(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId,
                                                           @Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("sId") Integer sId) {
        try {
            return new ResponseEntity<>(playlistService.deleteSongFromPlaylist(pId, sId, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Songs> getSongsOfSpecificPlaylist(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId) {

        try {
            return new ResponseEntity<>(playlistService.getSongsOfSpecificPlaylist(pId, getAuthenticatedUser().getName(), isAdminUser()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
        
    
}
