package io.swagger.api;

import io.swagger.model.Playlist;
import io.swagger.model.Playlists;
import io.swagger.model.Songs;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-11-26T17:46:13.799Z")


public class PlaylistsApiController implements PlaylistsApi {

    private static final Logger log = LoggerFactory.getLogger(PlaylistsApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public PlaylistsApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Playlist> getPlaylistById(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Playlist>(objectMapper.readValue("{  \"name\" : \"name\",  \"id\" : 0}", Playlist.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Playlist>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Playlist>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Songs> getSongsOfSpecificPlaylist(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Songs>(objectMapper.readValue("\"\"", Songs.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Songs>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Songs>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Playlists> playlistsGet() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Playlists>(objectMapper.readValue("\"\"", Playlists.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Playlists>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Playlists>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> playlistsPIdDelete(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> playlistsPIdPut(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId,@NotNull @ApiParam(value = "Name of Playlist", required = true) @Valid @RequestParam(value = "name", required = true) String name) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> playlistsPIdSongsSIdDelete(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId,@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("sId") Integer sId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> playlistsPIdSongsSIdPost(@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("pId") Integer pId,@Size(min=1) @ApiParam(value = "id",required=true) @PathVariable("sId") Integer sId) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Playlist> playlistsPost(@NotNull @ApiParam(value = "Name of Playlist", required = true) @Valid @RequestParam(value = "name", required = true) String name) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Playlist>(objectMapper.readValue("{  \"name\" : \"name\",  \"id\" : 0}", Playlist.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Playlist>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Playlist>(HttpStatus.NOT_IMPLEMENTED);
    }

}
