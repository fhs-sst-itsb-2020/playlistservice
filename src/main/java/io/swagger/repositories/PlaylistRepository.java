package io.swagger.repositories;

import io.swagger.domain.PlaylistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PlaylistRepository extends JpaRepository<PlaylistEntity, Long> {
	PlaylistEntity findById(Integer id);
	PlaylistEntity findByIdAndUsername(Integer id, String username);
	List<PlaylistEntity> findAllByUsername(String username);
}