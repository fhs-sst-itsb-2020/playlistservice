package io.swagger.repositories;

import io.swagger.domain.PlaylistEntity;
import io.swagger.domain.PlaylistSongEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PlaylistSongsRepository extends JpaRepository<PlaylistSongEntity, Integer> {

	PlaylistSongEntity findBysIdAndPlaylist(Integer sid, PlaylistEntity playlist);
    List<PlaylistSongEntity> findAllByPlaylist(PlaylistEntity playlist);

}