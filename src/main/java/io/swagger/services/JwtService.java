package io.swagger.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public interface JwtService {

    Jws<Claims> parseAndValidate(String jwt) throws InvalidKeySpecException, NoSuchAlgorithmException;

}
