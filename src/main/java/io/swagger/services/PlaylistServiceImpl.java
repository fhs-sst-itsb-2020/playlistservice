package io.swagger.services;

import io.swagger.api.NotAuthorizedException;
import io.swagger.api.NotFoundException;
import io.swagger.domain.*;
import io.swagger.model.*;
import io.swagger.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;


@Service
public class PlaylistServiceImpl implements PlaylistService {

	@Autowired
	PlaylistRepository playlistRepository;
	@Autowired
	PlaylistSongsRepository playlistsongsRepository;

	public PlaylistEntity getPlaylistAndCheckPermissions(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = playlistRepository.findById(pId);

		if (playlist == null) {
			throw new NotFoundException(404, "Playlist not found!");
		} else if (!adminUser && !playlist.getUsername().equalsIgnoreCase(username)) {
			throw new NotAuthorizedException(401, "Not authorized!");
		}

		return playlist;
	}


	private Playlist setPlaylistData(PlaylistEntity playlist) {
		Playlist ret = new Playlist();
		ret.setId(playlist.getId());
		ret.setName(playlist.getName());

		List<BigDecimal> bigDecimalList = new LinkedList<>();
		for (PlaylistSongEntity value : playlist.songs) {
			bigDecimalList.add(new BigDecimal(value.sId));
		}

		return ret;
	}


	@Override
	public Playlist getPlaylistById(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		return setPlaylistData(playlist);
	}


	@Override
	public Playlist addPlaylist(String name, String username) {
		PlaylistEntity playlist = new PlaylistEntity();
		playlist.setName(name);
		playlist.setUsername(username);
		playlistRepository.save(playlist);
		Playlist ret = new Playlist();
		ret.setId(playlist.getId());
		ret.setName(playlist.getName());
		return ret;
	}


	/**
	 * Diese Funktion fuegt einen neuen Song zu einer bestehenden Playlist hinzu.
	 * Es wird auch automatisch geprueft, ob ein User das Recht hat, bei der Playlist
	 * einen Song hinzuzufügen.
	 * @param pId Eindeutige ID der Playlist
	 * @param sId Eindeutige ID des Songs
	 * @param username Eindeutiges Kennzeichen des Users
	 * @param adminUser Kennzeichen, ob es ein Admin-User ist oder nicht
	 * @return
	 * @throws NotFoundException
	 */
	@Override
	public Void addSongToPlaylist(Integer pId, Integer sId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		PlaylistSongEntity songZuPlaylist = new PlaylistSongEntity();
		songZuPlaylist.setPlaylist(playlist);
		songZuPlaylist.sId = sId;
		playlistsongsRepository.save(songZuPlaylist);
		return null;
	}



	@Override
	public Void deletePlaylist(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		playlistRepository.delete(playlist);
		return null;
	}


	@Override
	public Playlists getAllPlaylists() {
		List<PlaylistEntity> playlists = playlistRepository.findAll();
		Playlists ret = new Playlists();

		for(PlaylistEntity p : playlists) {
			Playlist play = setPlaylistData(p);
			ret.add(play);
		}

		return ret;
	}

	@Override
	public Void changePlaylist(Integer pId, String name, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		playlist.setName(name);
		playlistRepository.save(playlist);
		return null;
	}

	@Override
	public Void deleteSongFromPlaylist(Integer pId, Integer sId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		PlaylistSongEntity song = playlistsongsRepository.findBysIdAndPlaylist(sId, playlist);
		playlistsongsRepository.delete(song);
		return null;
	}


	private Songs getSongsOfPlaylist(PlaylistEntity playlistEntity) {
		List<PlaylistSongEntity> song = playlistsongsRepository.findAllByPlaylist(playlistEntity);
		Songs ret = new Songs();

		for(PlaylistSongEntity s : song){
			ret.add(BigDecimal.valueOf(s.sId));
		}

		return ret;
	}


	@Override
	public Songs getSongsOfSpecificPlaylist(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException {
		PlaylistEntity playlist = getPlaylistAndCheckPermissions(pId, username, adminUser);
		return getSongsOfPlaylist(playlist);
	}


	@Override
	public Playlists getAllPlaylistsOfUser(String user) {
		List<PlaylistEntity> playlists = playlistRepository.findAllByUsername(user);
		Playlists ret = new Playlists();

		for(PlaylistEntity p : playlists) {
			Playlist play = setPlaylistData(p);
			ret.add(play);
		}

		return ret;
	}

}
