package io.swagger.services;

import io.swagger.api.NotAuthorizedException;
import io.swagger.api.NotFoundException;
import io.swagger.model.*;

public interface PlaylistService {

    public Playlist getPlaylistById(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	public Playlist addPlaylist(String name, String username);
	public Void addSongToPlaylist(Integer pId, Integer sId, String name, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	public Void deletePlaylist(Integer pId, String name, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	public Playlists getAllPlaylists();
	public Void changePlaylist(Integer pId, String name, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	public Void deleteSongFromPlaylist(Integer pId, Integer sId, String name, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	public Songs getSongsOfSpecificPlaylist(Integer pId, String username, boolean adminUser) throws NotFoundException, NotAuthorizedException;
	Playlists getAllPlaylistsOfUser(String user);

}