# Example multi-stage Dockerfile for Gradle and Java.
# In this example we use 2 stages:
#    - Build & Test: Builds our Java application using gradle and executes the unit tests.
#    - Runtime: Packages the application built in the previous stage together with the Java runtime.
# The advantage of this approach is that the final runtime image doesn't contain the build tools.
# Therefore, the image is smaller and more secure!

#################################################################
# Stage 1: BUILD & TEST                                         #
#################################################################
FROM maven:3.6.1-jdk-8-alpine AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package




#################################################################
# Stage 2: RUNTIME                                              #
#################################################################
FROM openjdk:8-jre-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
EXPOSE 8081
COPY --from=build /home/app/target/swagger-spring-1.0.0.jar /usr/local/lib/demo.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/demo.jar"]


