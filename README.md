# Funktionalitäten
PlaylistService bietet folgende Funktionen:

-	Das Anlegen einer Playlist
-	Das Löschen einer Playlist
-	Das Ändern einer Playlist
-	Das Abfragen von Playlists
-	Das Hinzufügen von Songs zu einer Playlist
-	Das Löschen von Songs aus einer Playlist

# Schnittstellenbeschreibung
Der Service wurde mithilfe von Swagger erstellt. Die oben genannten Funktionen werden durch eine API-Schnittstelle zur Verfügung gestellt und sind unter [swagger.yml](swagger/swagger.yaml) genauer beschrieben. Dort kann die Schnittstelle auch getestet werden.
Kurzbeschreibung der Schnittstellen:

- `GET /playlists` --> liefert alle für den User verfügbaren Playlists
- `POST /playlists` --> erstellt eine neue Playlist
- `GET /playlists/{pId}` --> liefert die Playlist mit der entsprechenden pId
- `PUT /playlists/{pId}` --> ändert die Playlist mit der entsprechenden pId
- `DELETE /playlists/{pId}` --> löscht die Playlist mit der entsprechenden pId
- `GET /playlists/{pId}/songs` --> liefert alle Songs aus der Playlist pId
- `POST /playlists/{pId}/songs/{sId}` --> fügt Song sId zur Playlist pId hinzu
- `DELETE /playlists/{pId}/songs/{sId}` --> löscht Song sId aus Playlist pId
